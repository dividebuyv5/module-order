<?php

declare(strict_types=1);

namespace Dividebuy\Order\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Magento\Framework\DataObject;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\SessionHelper;
use Dividebuy\Common\Quote;
use Exception;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;

class Create extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected Onepage $onePage;
  protected ApiHelper $apiHelper;
  private StoreManagerInterface $storeManager;
  private ResponseHelper $responseHelper;
  private SessionHelper $sessionHelper;
  private CartHelper $cartHelper;

  public function __construct(
      Context $context,
      Onepage $onePage,
      StoreManagerInterface $storeManager,
      ApiHelper $apiHelper,
      ResponseHelper $responseHelper,
      SessionHelper $sessionHelper,
      CartHelper $cartHelper
  ) {
    $this->onePage = $onePage;
    $this->sessionHelper = $sessionHelper;
    $this->storeManager = $storeManager;
    $this->apiHelper = $apiHelper;
    $this->responseHelper = $responseHelper;
    $this->cartHelper = $cartHelper;

    parent::__construct($context);
  }

  /**
   * Used to create an order.
   *
   * @return ResponseInterface|ResultInterface|void
   *
   * @throws Exception
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $splashKey = $this->getRequest()->getParam('authentication_splash_key');
    $storeTokenAndAuthentication = explode(':', base64_decode($splashKey));

    if (empty($storeTokenAndAuthentication[0]) && empty($storeTokenAndAuthentication[1])) {
      $this->messageManager->addError(__('Authentication Failed.'));
      $response = $this->getRedirectErrorResponse($this->cartHelper->getCartUrl());

      return $this->responseHelper->sendJsonResponse($response);
    }

    //verify retailer token and authentication
    $authenticationStatus = $this->responseHelper->getConfigHelper()->isFailedAuth(
        $storeTokenAndAuthentication[0],
        $storeTokenAndAuthentication[1]
    );

    if ($authenticationStatus !== false) {
      return $this->responseHelper->sendJsonResponse($authenticationStatus);
    }

    //checking for non divide buy product
    $checkCart = $this->cartHelper->getCartItems();

    if (isset($checkCart['nodividebuy']) && $checkCart['nodividebuy'] !== 0) {
      $this->messageManager->addError(__('You still have Non-DivideBuy products in cart'));
      $response = $this->getRedirectErrorResponse($this->cartHelper->getCartUrl());
      $this->responseHelper->debugResponse($response);

      return $this->responseHelper->sendJsonResponse($response);
    }

    $zipcode = $this->getRequest()->getParam('postcode');
    // Checking if DivideBuy ships has allowed to ship items for entered postcode by user.
    $zipcodeVerification = $this->responseHelper->getConfigHelper()->isPostCodeDeliverable((string) $zipcode);

    if (!$zipcodeVerification) {
      return $this->getInvalidPostcodeResponse();
    }

    $shippingMethodCode = $this->getRequest()->getParam('shipping_method');
    $userEmail = $this->getRequest()->getParam('user_email');
    $customerEmail = $this->getRequest()->getParam('customerEmail');
    $this->sessionHelper->setInstallment($this->getRequest()->getParam('selected_instalment'));

    /** @var Quote $quote */
    $quote = $this->onePage->getQuote();

    // Assign customer firstname, lastname and email which are added from retailer checkout page.
    if ($this->sessionHelper->isLoggedIn()) {
      $customer = $this->sessionHelper->getCustomer();
      $firstName = $customer->getFirstname();
      $lastName = $customer->getLastname();
      $email = $customer->getEmail();
    } elseif ($customerEmail && filter_var($customerEmail, FILTER_VALIDATE_EMAIL)) {
      $userShippingAddress = $quote->getShippingAddress()->getData();
      $quote->setCustomerFirstname($userShippingAddress['firstname']);
      $quote->setCustomerLastname($userShippingAddress['lastname']);
      $quote->setCustomerEmail($customerEmail);

      $firstName = $quote->getCustomerFirstname();
      $lastName = $quote->getCustomerLastname();
      $email = $quote->getCustomerEmail();
      if(empty($firstName)){
        $firstName = $quote->getCustomerFirstname() ?? DivideBuy::DEFAULT_FIRST_NAME;
      }
      if(empty($lastName)){
        $lastName = $quote->getCustomerLastname() ?? DivideBuy::DEFAULT_LAST_NAME;
      }
    } else {
      $firstName = $quote->getCustomerFirstname() ?? DivideBuy::DEFAULT_FIRST_NAME;
      $lastName = $quote->getCustomerLastname() ?? DivideBuy::DEFAULT_LAST_NAME;
      $email = $quote->getCustomerEmail() ?? DivideBuy::DEFAULT_EMAIL;
    }

    $billingAddressArray = $this->buildAddressArray(
        $quote->getBillingAddress()->getData(),
        [
          'firstname' => $firstName,
          'lastname' => $lastName,
          'email' => $email,
          'country_id' => 'GB',
          'postcode' => $zipcode,
        ]
    );
    
    $shippingAddressArray = $this->buildAddressArray(
        $quote->getShippingAddress()->getData(),
        [
          'firstname' => $firstName,
          'lastname' => $lastName,
          'email' => $email,
          'country_id' => 'GB',
          'postcode' => $zipcode,
        ]
    );

    // Set Sales Order Billing Address
    $quote->getBillingAddress()->addData($billingAddressArray);

    // Set Sales Order Shipping Address
    $shippingAddress = $quote->getShippingAddress()->addData($shippingAddressArray);

    // Collect Rates and Set Shipping & Payment Method
    $shippingAddress->setCollectShippingRates(false)
        ->collectShippingRates()
        ->setShippingMethod($shippingMethodCode); //shipping method

    $quote->setPaymentMethod(DivideBuy::DIVIDEBUY_PAYMENT_CODE); //payment method
    // Set Sales Order Payment
    $quote->getPayment()->importData(['method' => DivideBuy::DIVIDEBUY_PAYMENT_CODE]);

    // Collect Totals & Save Quote
    $quote->collectTotals()->save();

    if (!empty($userEmail)) {
      // Storing value of user email into session.
      $this->sessionHelper->setUserEmail($userEmail);
    }
    try {
      $this->onePage->saveOrder();

      // Load last created order.
      $orderId = $this->sessionHelper->getLastOrderId();
      $order = $this->cartHelper->getOrderById($orderId);
      $storeId = $order->getStoreId();
      $splashKey = $this->responseHelper->getConfigHelper()->generateSplashKey($orderId);
      $phoneOrderToken = $this->sessionHelper->getPhoneOrderToken();
      $redirectUrl = $this->apiHelper->getSplashRedirectUrl(
          $this->storeManager->getStore()->getId(),
          $splashKey,
          $phoneOrderToken
      );

      // Getting value of DivideBuy user email.
      $divideBuyUserEmail = $this->sessionHelper->getUserEmail();

      // Sending order details to DivideBuy core API.
      if ($divideBuyUserEmail) {
        $request = [
            'orderId' => $orderId,
            'retailerId' => $this->responseHelper->getConfigHelper()->getRetailerId(),
            'storeToken' => $this->responseHelper->getConfigHelper()->getAuthenticationKey(),
            'storeAuthentication' => $this->responseHelper->getConfigHelper()->getAuthenticationKey(),
            'email' => $divideBuyUserEmail,
            'name' => '',
            'token' => $phoneOrderToken,
        ];

        $request = $this->responseHelper->jsonEncode($request);
        $url = $this->apiHelper->getPosApiEndpoint($storeId);
        $this->apiHelper->sendRequest($url, $request);
      }

      $order->setState(Order::STATE_PENDING_PAYMENT)->setStatus(DivideBuy::DIVIDEBUY_ORDER_STATUS);
      $order->save();

      // Checking if user email value is not null.
      if ($userEmail && $phoneOrderToken) {
        $result = $this->getPhoneOrderTokenResponse();
      } elseif ($userEmail) {
        $result = $this->getRedirectResponse(true, $this->storeManager->getStore()->getBaseUrl());
      } else {
        $result = $this->getRedirectErrorResponse($redirectUrl);
      }

      // Truncate cart after order placement.
      $this->cartHelper->truncateCart();

      // Restoring non DivideBuy products into cart.
      if ($this->sessionHelper->hasTemporaryCart()) {
          $this->sessionHelper->addSessionProducts();
      }

      //restore cart for adding the non divide buy products
      $this->sessionHelper->restoreQuote();
      $quoteItemArray = $this->cartHelper->getCartItems();
      $divideBuyIds = $quoteItemArray['dividebuyIds'] ?? [];

      $this->cartHelper->removeItemsById((array) $divideBuyIds);

      return $this->responseHelper->sendJsonResponse($result);
    } catch (Exception $exception) {
      return $this->returnExceptionResponse($exception);
    }
  }

  public function getInvalidPostcodeResponse()
  {
    $this->messageManager->addError(__('Unfortunately we are unable to deliver to this postcode. Please call our Customer Service Team for more details: 0800 085 0885.'));
    $response = $this->getRedirectErrorResponse($this->cartHelper->getCartUrl());
    $this->responseHelper->debugResponse($response);

    return $this->responseHelper->sendJsonResponse($response);
  }

  public function getPhoneOrderTokenResponse(): array
  {
    $store = $this->storeManager->load('default', 'code');
    $storeId = $store->getId();
    $portalUrl = $this->apiHelper->getPortalUrl($storeId);
    $this->sessionHelper->unsetCheckoutSessionAndToken();

    return $this->getRedirectResponse(true, $portalUrl);
  }

  public function returnExceptionResponse($exception)
  {
    if ($this->sessionHelper->hasTemporaryCart()) {
      $this->sessionHelper->addSessionProducts();
    }
    $response = $this->getRedirectErrorResponse($this->cartHelper->getCartUrl());

    $this->responseHelper->debugResponse($response, __METHOD__, $exception);

    return $this->responseHelper->sendJsonResponse($response);
  }

  private function buildAddressArray($data, array $params = []): array
  {
    $quote = $this->onePage->getQuote();
    $userShippingAddress = $quote->getShippingAddress()->getData();

    // for Shipping Address street
    if (!empty($userShippingAddress['street'])) {
        $shippingStreet = $userShippingAddress['street'];
    } else {
        $shippingStreet = array(
            '0' => 'Brindley Court',
            '1' => 'Lymedale Business Park',
        );
    }

    // for Shipping Address city
    if (!empty($userShippingAddress['city'])) {
        $shippingCity = $userShippingAddress['city'];
    } else {
        $shippingCity = 'Staffordshire';
    }

    //for Shipping Address region
    if (!empty($userShippingAddress['region'])) {
        $shippingRegion = $userShippingAddress['region'];
    } else {
        $shippingRegion = 'Staffordshire';
    }

    //for Shipping Address telephone
    if (!empty($userShippingAddress['telephone'])) {
      $shippingTelephone = $userShippingAddress['telephone'];
    } else {
      $shippingTelephone = '08000850885';
    }

    $dataObject = new DataObject($data);
    return array_merge($params, [
        'street' => $shippingStreet,
        'city' => $shippingCity,
        'country_id' => 'GB',
        'region' => $shippingRegion,
        'telephone' => $shippingTelephone,
    ]);
  }
}
