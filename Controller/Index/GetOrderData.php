<?php

declare(strict_types=1);

namespace Dividebuy\Order\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\SessionHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class GetOrderData extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected ResponseHelper $responseHelper;

  private SessionHelper $sessionHelper;

  public function __construct(Context $context, ResponseHelper $responseHelper, SessionHelper $sessionHelper)
  {
    $this->responseHelper = $responseHelper;
    $this->sessionHelper = $sessionHelper;

    parent::__construct($context);
  }

  /**
   * Used to get zipcode and shipping method of current quote.
   *
   * @return ResponseInterface|ResultInterface|void
   */
  public function execute()
  {
    $quote = $this->sessionHelper->getQuote();
    $zipcode = $quote->getShippingAddress()->getPostcode();
    $quoteShipMethod = $quote->getShippingAddress()->getShippingMethod();

    $result = [
        'zipcode' => $zipcode,
        'shippingMethod' => $quoteShipMethod,
    ];

    return $this->responseHelper->sendJsonResponse($result);
  }
}
